import {Component, Input} from "@angular/core";
import {Location} from '@angular/common';

import {Movie} from "../shared/movie.model";
import {MovieService} from "../shared/movie.service";


@Component({
    moduleId: module.id,
    selector: 'ubb-movie-new',
    templateUrl: './movie-new.component.html',
    styleUrls: ['./movie-new.component.css'],
})
export class MovieNewComponent {
    @Input() movie: Movie;

    constructor(private movieService: MovieService,
                private location: Location) {
    }

    goBack(): void {
        this.location.back();
    }


    save(movieId, title, author): void {
        console.log("movie: ", movieId, title, author);
        if (!this.isValid(movieId, title, author)) {
            console.log("all fields are required ");
            alert("all fields are required");
            return;
        }
        this.movieService.create(movieId, title, author)
            .subscribe(_ => this.goBack());
    }

    private isValid(movieId, title, author) {
        if (!movieId || !title || !author) {
            console.log("all fields are required");
            return false;
        }
        return true;
    }
}