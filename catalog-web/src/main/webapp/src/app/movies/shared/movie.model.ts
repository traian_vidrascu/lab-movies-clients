export class Movie{
    id: number;
    movieId: string;
    title: string;
    author: string;
}