import {Component, Input} from "@angular/core";
import {Location} from '@angular/common';

import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";


@Component({
    moduleId: module.id,
    selector: 'ubb-client-new',
    templateUrl: './client-new.component.html',
    styleUrls: ['./client-new.component.css'],
})
export class ClientNewComponent {
    @Input() client: Client;

    constructor(private clientService: ClientService,
                private location: Location) {
    }

    goBack(): void {
        this.location.back();
    }


    save(cnp, firstName, lastName): void {
        console.log("client: ", cnp, firstName, lastName);
        if (!this.isValid(cnp, firstName, lastName)) {
            console.log("all fields are required ");
            alert("all fields are required");
            return;
        }
        this.clientService.create(cnp, firstName, lastName)
            .subscribe(_ => this.goBack());
    }

    private isValid(cnp, firstName, lastName) {
        if (!cnp || !firstName || !lastName) {
            console.log("all fields are required");
            return false;
        }
        //TODO other validations
        return true;
    }
}