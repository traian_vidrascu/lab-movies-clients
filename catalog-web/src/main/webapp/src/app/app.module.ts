import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ClientsComponent} from "./clients/clients.component";
import {ClientListComponent} from "./clients/client-list/client-list.component";
import {ClientService} from "./clients/shared/client.service";
import {ClientDetailComponent} from "./clients/client-detail/client-detail.component";
import {MoviesComponent} from "./movies/movies.component"
import {MovieDetailComponent} from "./movies/movie-detail/movie-detail.component"
import {MovieListComponent} from "./movies/movie-list/movie-list.component";
import {MovieService} from "./movies/shared/movie.service";
import {ClientNewComponent} from "./clients/client-new/client-new.component";
import {MovieNewComponent} from "./movies/movie-new/movie-new.component";




@NgModule({
    declarations: [
        AppComponent,

        ClientsComponent,
        ClientListComponent,
        ClientDetailComponent,
        ClientNewComponent,

        MoviesComponent,
        MovieListComponent,
        MovieDetailComponent,
        MovieNewComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
    ],
    providers: [ClientService,MovieService],
    bootstrap: [AppComponent]
})
export class AppModule {
}


