package ro.ubb.catalog.web.dto;

import lombok.*;
import ro.ubb.catalog.core.model.Client;

import java.util.List;
import java.util.Set;

public class ClientsDto {


    Set<Client> clients;

    public ClientsDto(Set<Client> clients) {
        this.clients = clients;
    }

    public ClientsDto() {

    }

    public Set<Client> getClients() {
        return clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return "ClientsDto{" +
                "clients=" + clients +
                '}';
    }
}
