package ro.ubb.catalog.web.dto;

import lombok.*;
import ro.ubb.catalog.core.model.Movie;

import java.util.List;
import java.util.Set;

/**
 * Created by Traian on 17.04.2017.
 */

public class MoviesDto {
    private Set<Movie> movies;

    public MoviesDto(Set<Movie> movies) {
        this.movies = movies;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "MoviesDto{" +
                "movies=" + movies +
                '}';
    }
}
