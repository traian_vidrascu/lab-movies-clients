package ro.ubb.catalog.web.dto;

/**
 * Created by Traian on 28.04.2017.
 */
public class MovieDto extends BaseDto{
    private String movieId;
    private String title;
    private String author;

    public MovieDto() {
    }

    public MovieDto(String movieId, String title, String author) {
        this.movieId = movieId;
        this.title = title;
        this.author = author;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "MovieDto{" +
                "movieId='" + movieId + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}' + super.toString();
    }
}
