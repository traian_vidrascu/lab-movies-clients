package ro.ubb.catalog.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Traian on 27.04.2017.
 */
@JsonSerialize
public class EmptyJsonResponse {
}
