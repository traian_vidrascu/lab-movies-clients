package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;

/**
 * Created by Traian on 28.04.2017.
 */
@Component
public class ClientConverter extends BaseConverter<Client,ClientDto>{

    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto clientDto = new ClientDto(client.getCnp(), client.getFirstName(), client.getLastName());
        clientDto.setId(client.getId());
        return clientDto;
    }
}
