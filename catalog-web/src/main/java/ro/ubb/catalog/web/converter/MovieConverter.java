package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.web.dto.BaseDto;
import ro.ubb.catalog.web.dto.MovieDto;

/**
 * Created by Traian on 28.04.2017.
 */
@Component
public class MovieConverter extends BaseConverter<Movie,MovieDto> {
    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public MovieDto convertModelToDto(Movie movie) {
        MovieDto movieDto = new MovieDto(movie.getMovieId(), movie.getAuthor(), movie.getTitle());
        movieDto.setId(movie.getId());
        return movieDto;
    }
}
