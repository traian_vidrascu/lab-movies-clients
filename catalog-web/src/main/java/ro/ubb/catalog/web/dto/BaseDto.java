package ro.ubb.catalog.web.dto;
import lombok.*;
import java.io.Serializable;

/**
 * Created by Traian on 27.04.2017.
 */

public class BaseDto implements Serializable {
    private Long id;

    public BaseDto() {
    }

    public BaseDto(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseDto{" +
                "id=" + id +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
