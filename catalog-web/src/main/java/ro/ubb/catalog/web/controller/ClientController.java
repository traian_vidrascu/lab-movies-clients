package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by radu.
 */

@RestController
public class ClientController {

    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
    public ClientsDto getClients() {
        log.trace("getClients");

        Set<Client> clients = clientService.findAll();

        log.trace("getClients: clients={}", clients);

        return new ClientsDto(clients);
    }

    @RequestMapping(value = "/clients/add",method = RequestMethod.POST)
    public Map<String,ClientDto> createClient(@RequestBody final Map<String,ClientDto> clientDtoMap){
        log.trace("createClient: clientDtoMap={}",clientDtoMap);
        ClientDto clientDto = clientDtoMap.get("client");
        Client client = clientService.createClient(clientDto.getCnp(),clientDto.getFirstName(),clientDto.getLastName());
        Map<String,ClientDto> result = new HashMap<>();
        result.put("client",clientConverter.convertModelToDto(client));
        log.trace("createClient: result={}",result);
        return result;
    }

    @RequestMapping(value = "/clients/{clientId}",method = RequestMethod.DELETE)
    public ResponseEntity deleteClient(@PathVariable final Long clientId){
        log.trace("deleteClient: clientId={},clientDtoMap={}",clientId);
        clientService.removeClient(clientId);
        log.trace("deleteClient: done");

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{clientId}",method = RequestMethod.PUT)
    public Map<String,ClientDto> updateClient(
            @PathVariable final long clientId,
            @RequestBody final Map<String,ClientDto> stringClientDtoMap){
        log.trace("updateClient: clientId={},stringClientDtoMap={}",clientId,stringClientDtoMap);
        ClientDto clientDto = stringClientDtoMap.get("client");
        Client client = clientService.updateClient(clientDto.getId(),clientDto.getCnp(),clientDto.getFirstName(),
                                                    clientDto.getLastName());
        HashMap<String,ClientDto> result = new HashMap<>();
        result.put("client",clientDto);
        log.trace("client={}",clientDto);
        return result;
    }

}
