package ro.ubb.catalog.web.dto;

import lombok.*;

/**
 * Created by Traian on 27.04.2017.
 */
public class ClientDto extends BaseDto {
    private String cnp;
    private String firstName;
    private String lastName;

    public ClientDto() {
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ClientDto(String cnp, String first_name, String last_name) {
        this.cnp=cnp;
        this.firstName=first_name;
        this.lastName=last_name;

    }

    @Override
    public String toString(){
        return "ClientDto{"+
                "cnp='"+cnp+"',"+
                "firstName='"+firstName+"'"+
                "lastName='"+lastName+"'}" + super.toString();
    }
}
