package ro.ubb.catalog.web.controller;

import com.sun.org.apache.regexp.internal.RE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.service.MovieService;
import ro.ubb.catalog.web.converter.MovieConverter;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.MoviesDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Traian on 17.04.2017.
 */
@RestController
public class MovieController {
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private MovieService movieService;
    @Autowired
    private MovieConverter movieConverter;

    @RequestMapping(value = "/movies",produces = MediaType.APPLICATION_JSON_VALUE)
    public MoviesDto getMovies(){
        log.trace("getMovies");

        Set<Movie> movies = movieService.findAll();

        log.trace("getMovies: movies={}",movies);

        return new MoviesDto(movies);
    }

    @RequestMapping(value = "/movies/add",method = RequestMethod.POST)
    public Map<String,MovieDto> createMovie(@RequestBody final Map<String,MovieDto> stringMovieDtoMap){
        log.trace("createMovie: stringMovieDtoMap={}",stringMovieDtoMap);
        MovieDto movieDto = stringMovieDtoMap.get("movie");
        Movie movie = movieService.createMovie(movieDto.getMovieId(),movieDto.getTitle(),movieDto.getAuthor());
        HashMap<String,MovieDto> result = new HashMap<>();
        result.put("movie",movieConverter.convertModelToDto(movie));
        log.trace("createMovie: movie={},result={}",movie,result);
        return result;
    }

    @RequestMapping(value = "/movies/{movieId}",method = RequestMethod.PUT)
    public Map<String,MovieDto> updateMovie(@PathVariable final long movieId,
                                            @RequestBody final Map<String,MovieDto> stringMovieDtoMap){
        log.trace("updateMovie: movieId={},stringMovieDtoMap={}",movieId,stringMovieDtoMap);
        MovieDto movieDto = stringMovieDtoMap.get("movie");
        Movie movie = movieService.updateMovie(movieId,movieDto.getMovieId(),movieDto.getTitle(),movieDto.getAuthor());
        HashMap<String,MovieDto> result = new HashMap<>();
        result.put("movie",movieConverter.convertModelToDto(movie));
        log.trace("updateMovie: movie={},result={}",movie,result);
        return result;
    }

    @RequestMapping(value = "/movies/{movieId}",method = RequestMethod.DELETE)
    public ResponseEntity deleteMovie(@PathVariable final long movieId){
        log.trace("deleteMovie: movieId={}",movieId);
        movieService.deleteMovie(movieId);
        log.trace("deleteMovie: end");
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }


}
