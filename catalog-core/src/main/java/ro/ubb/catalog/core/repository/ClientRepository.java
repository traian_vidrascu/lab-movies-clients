package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Client;

/**
 * Created by radu.
 */


public interface ClientRepository extends CatalogRepository<Client, Long> {

}
