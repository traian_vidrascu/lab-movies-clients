package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Movie;

/**
 * Created by Traian on 17.04.2017.
 */
public interface MovieRepository extends CatalogRepository<Movie,Long> {
}
