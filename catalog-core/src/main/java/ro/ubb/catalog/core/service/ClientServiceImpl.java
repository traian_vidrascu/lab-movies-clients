package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Set<Client> findAll() {
        log.trace("findAll");

        Set<Client> clients = clientRepository.findAll().stream().distinct().collect(Collectors.toSet());

        log.trace("findAll: clients={}", clients);

        return clients;
    }

    @Override
    @Transactional
    public Client createClient(String cnp, String first_name, String last_name) {
        log.trace("createClient: cnp={},firstName={},lastName={}",cnp,first_name,last_name);
        Client client = new Client(cnp,first_name,last_name);
        client = clientRepository.save(client);
        log.trace("client={}",client);
        return client;
    }

    @Override
    @Transactional
    public Client updateClient(Long id,String cnp, String first_name, String last_name) {
        log.trace("updateClient: id={},cnp={},firstName={},lastName={}",id,cnp,first_name,last_name);
        Client client = clientRepository.findOne(id);
        client.setCnp(cnp);
        client.setFirstName(first_name);
        client.setLastName(last_name);
        log.trace("Client: client={}",client);
        return client;
    }

    @Override
    @Transactional
    public void removeClient(long id) {
        log.trace("removeClient: id={}",id);
        clientRepository.delete(id);
        log.trace("end");
    }
}
