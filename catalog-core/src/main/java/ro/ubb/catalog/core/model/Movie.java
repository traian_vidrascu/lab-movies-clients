package ro.ubb.catalog.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Traian on 17.04.2017.
 */
@Entity
@Table(name = "movie")
public class Movie extends BaseEntity<Long> {
    @Column(name = "movie_id",nullable = false,unique = true)
    private String movieId;

    @Column(name = "title",nullable = false)
    private String title;

    @Column(name = "author",nullable = false)
    private String author;

    public Movie() {
    }

    public Movie(String movieID, String title, String author) {
        this.movieId = movieID;
        this.title = title;
        this.author = author;
    }

    public String getMovieId() {

        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (!movieId.equals(movie.movieId)) return false;
        if (!title.equals(movie.title)) return false;
        return author.equals(movie.author);
    }

    @Override
    public int hashCode() {
        int result = movieId.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }
}
