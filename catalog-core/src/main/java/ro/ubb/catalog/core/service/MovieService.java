package ro.ubb.catalog.core.service;

import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Movie;

import java.util.List;
import java.util.Set;

/**
 * Created by Traian on 17.04.2017.
 */

public interface MovieService {
    Set<Movie> findAll();
    Movie createMovie(String movieId,String title,String author);
    void  deleteMovie(Long id);
    Movie updateMovie(Long id,String movieId,String title,String author);
}
