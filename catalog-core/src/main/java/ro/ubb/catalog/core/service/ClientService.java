package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Client;

import java.util.List;
import java.util.Set;

/**
 * Created by radu.
 */
public interface ClientService {
    Set<Client> findAll();
    Client createClient(String cnp, String first_name, String last_name);
    Client updateClient(Long id,String cnp, String first_name, String last_name);
    void removeClient(long id);
}
