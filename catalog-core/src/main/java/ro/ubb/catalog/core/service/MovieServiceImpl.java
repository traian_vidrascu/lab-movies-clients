package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.repository.MovieRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Traian on 17.04.2017.
 */
@Service
public class MovieServiceImpl implements MovieService {
    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public Set<Movie> findAll() {
        log.trace("findAll");

        Set<Movie>  movies = movieRepository.findAll().stream().distinct().collect(Collectors.toSet());

        log.trace("findAll: movies={}",movies);

        return movies;
    }
    @Transactional
    @Override
    public Movie createMovie(String movieId, String title, String author) {
        log.trace("createMovie: movieId={},title={},author={}",movieId,title,author);
        Movie movie = new Movie(movieId,title,author);
        movie = movieRepository.save(movie);
        log.trace("createMovie: movie={}",movie);
        return movie;
    }

    @Transactional
    @Override
    public void deleteMovie(Long id) {
        log.trace("deleteMovie: id={}",id);
        movieRepository.delete(id);
        log.trace("deletedMovie: end");
    }

    @Override
    @Transactional
    public Movie updateMovie(Long id, String movieId, String title, String author) {
        log.trace("updateMovie: id={},movieId={},title={},author={}",movieId,title,author);
        Movie movie = movieRepository.findOne(id);
        movie.setMovieId(movieId);
        movie.setTitle(title);
        movie.setAuthor(author);
        log.trace("updateMovie: movie={}",movie);
        return movie;
    }
}
